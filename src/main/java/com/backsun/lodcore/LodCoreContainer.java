package com.backsun.lodcore;

import java.util.Arrays;

import com.google.common.eventbus.EventBus;

import net.minecraftforge.fml.common.DummyModContainer;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.ModMetadata;

/**
 * This file is similar to mcmod.info
 * 
 * @author James Seibel
 * @version 02-07-2021
 */
public class LodCoreContainer extends DummyModContainer
{
    public LodCoreContainer()
    {
        super(new ModMetadata());
        ModMetadata meta = getMetadata();
        meta.modId = "lodcore";
        meta.name = "Lod Core";
        meta.description = "Required for the Lod mod";
        meta.version = "1.12.2";
        meta.authorList = Arrays.asList("James Seibel");
    }

    @Override
    public boolean registerBus(EventBus bus, LoadController controller)
    {
        bus.register(this);
        return true;
    }
}