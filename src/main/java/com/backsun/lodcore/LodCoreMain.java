package com.backsun.lodcore;

import java.util.Map;

import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;

/**
 * This Forge core mod is used by my lod
 * mod to build the stencil buffer in
 * some of Minecraft's vanilla code that
 * couldn't otherwise be accessed.
 * 
 * @author James Seibel
 * @version 02-07-2021
 */
@IFMLLoadingPlugin.MCVersion("1.12.2")
@IFMLLoadingPlugin.TransformerExclusions({"com.backsun.lodcore.asm"})
public class LodCoreMain implements IFMLLoadingPlugin
{
	@Override
	public String[] getASMTransformerClass()
	{
		return new String[]{"com.backsun.lodcore.asm.RenderGlobalClassTransformer"};
	}
	
	@Override
	public String getModContainerClass()
	{
		return "com.backsun.lodcore.LodCoreContainer";
	}
	
	@Override
	public String getSetupClass()
	{
		return null;
	}
	
	@Override
	public void injectData(Map<String, Object> data)
	{
		
	}
	
	@Override
	public String getAccessTransformerClass()
	{
		return null;
	}
}
